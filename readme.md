# EDA + Water Samples Clusters

## Data Scientist
- Ferrán, Natanael Emir

## Dataset
- [Contaminated River Water Parameters](https://www.kaggle.com/datasets/natanaelferran/river-water-parameters)
- **License:** [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
- **Authors:** Natanael Emir Ferrán, Nicolás Coronel, Martín Pastorini

## To resolve
- Find data patterns 

## Used technique
- K-Means
